'use strict';

const router = require('express').Router();
const Promise = require('bluebird');

const resErr = require('../utils/respond-error');
const middleware = require('../config/middleware');
const User = require('../models/user');
const Post = require('../models/post');

let localTemp = {};
const findPostByID = (postID) => {
	return Post
		.findById(postID)
		.exec()
};
const findPostsByUserID = (userID) => {
	return Post
		.find({userID})
		.sort({createdAt: -1})
		.exec()
};
const findPostsByParentPostID = (parentPostID) => {
	return Post
		.find({parentPostID})
		.sort({createdAt: -1})
		.exec()
};

router.param('postID', (req, res, next, postID) => {
	return findPostByID(req.params.postID)
		.then(post => {
			localTemp.post = post;
			return next();
		})
		.catch(err => resErr(res, err.status, err.message))
		.catch(next);
});
router.param('commentID', (req, res, next, commentID) => {
	return findPostByID(req.params.commentID)
		.then(comment => {
			localTemp.post.comment = comment;
			return next();
		})
		.catch(err => resErr(res, err.status, err.message))
		.catch(next);
});

router.route('/')
.get((req, res, next) => {
	return findPostsByUserID(req.locals.user._id)
		.then(posts => res.status(200).json(posts))
		.catch(err => resErr(res, err.status, err.message))
		.catch(next);
})
.post((req, res, next) => {
	req.body.userID = (req.locals.user.user)? req.locals.user.user._id : req.locals.user._id;
	return new Post(req.body)
		.save()
		.then(post => res.status(201).json(post))
		.catch(err => resErr(res, err.status, err.message))
		.catch(next);
});

router.route('/:postID')
.get((req, res, next) => res.status(200).json(localTemp.post))
.put(middleware.verifyUser, (req, res, next) => {
	return localTemp.post.comment
		.update(req.body)
		.then(comment => res.status(200).json(comment))
		.catch(err => resErr(res, err.status, err.message))
		.catch(next);
})
.delete(middleware.verifyUser, (req, res, next) => {
	return localTemp.post
		.remove()
		.then(comment => res.status(200).json({comment}))
		.catch(err => resErr(res, err.status, err.message))
		.catch(next);
});

router.route('/:postID/vote-:direction')
.all(middleware.validVote)
.get((req, res, next) => {
	return localTemp.post
		.vote(req.params.direction)
		.then(post => res.status(200).json(post))
		.catch(err => resErr(res, err.status, err.message))
		.catch(next);
})
.post((req, res, next) => {
	return localTemp.post
		.vote(req.params.direction)
		.then(post => res.status(200).json(post))
		.catch(err => resErr(res, err.status, err.message))
		.catch(next);
});

router.route('/:postID/comments')
.get((req, res, next) => {
	return findPostsByParentPostID(localTemp.post._id)
		.then(posts => res.status(200).json(posts))
		.catch(err => resErr(res, err.status, err.message))
		.catch(next);
})
.post((req, res, next) => {
	req.body.userID = localTemp.post.userID;
	req.body.parentPostID = localTemp.post._id;

	return new Post(req.body)
		.save()
		.then(post => res.status(201).json(post))
		.catch(err => resErr(res, err.status, err.message))
		.catch(next);
});

router.route('/:postID/comments/:commentID')
.get((req, res, next) => res.status(200).json(localTemp.post.comment))
.put(middleware.verifyUser, (req, res, next) => {
	return localTemp.post.comment
		.update(req.body)
		.then(comment => res.status(200).json(comment))
		.catch(err => resErr(res, err.status, err.message))
		.catch(next);
})
.delete(middleware.verifyUser, (req, res, next) => {
	return localTemp.post.comment
		.remove()
		.then(comment => res.status(200).json({comment}))
		.catch(err => resErr(res, err.status, err.message))
		.catch(next);
});

router.route('/:postID/comments/:commentID/vote-:direction')
.all(middleware.validVote)
.get((req, res, next) => {
	return localTemp.post.comment
		.vote(req.params.direction)
		.then(comment => res.status(200).json(comment))
		.catch(err => resErr(res, err.status, err.message))
		.catch(next);
})
.post((req, res, next) => {
	return localTemp.post.comment
		.vote(req.params.direction)
		.then(comment => res.status(201).json(comment))
		.catch(err => resErr(res, err.status, err.message))
		.catch(next);
});

module.exports = router;
